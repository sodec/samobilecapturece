/*
 * Copyright (C) 2020 Sodec Apps Bilisim Teknolojileri, Inc - All rights reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Hasan Dertli <hasan.dertli@sodecapps.com>
 *
 * Sodec Apps Bilisim Teknolojileri
 * http://sodecapps.com
 * support@sodecapps.com
 */

typedef void (^SAFaceRecognitionSuccess)(void);
typedef void (^SAFaceRecognitionFailure)(void);

@interface SAFaceRecognition : NSObject

@property (copy) SAFaceRecognitionSuccess onSuccess;
@property (copy) SAFaceRecognitionFailure onFailure;

@end
